from django.shortcuts import render
from models import Job, SideProject, Education, PersonalInfo
from django.http import JsonResponse


def index(request):
    jobs = Job.objects.all()
    projects = SideProject.objects.all()
    education = Education.objects.all()
    me = PersonalInfo.objects.all()[0]
    return render(request, 'resume/index.html', {
        "jobs": jobs,
        "projects": projects,
        "education": education,
        "me": me
    })


def json_view(request):
    json_out = {
        "me": PersonalInfo.objects.all()[0].serialize(),
        "education": [e.serialize() for e in Education.objects.all()],
        "jobs": [j.serialize() for j in Job.objects.all()],
        "projects": [p.serialize() for p in SideProject.objects.all()]
    }

    return JsonResponse(json_out)