# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('company_name', models.CharField(max_length=30)),
                ('logo_url', models.CharField(max_length=30)),
                ('project_title', models.CharField(max_length=50)),
                ('project_logo', models.CharField(max_length=50)),
                ('project_url', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='JobTag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=50)),
                ('job', models.ForeignKey(to='resume.Job')),
            ],
        ),
        migrations.CreateModel(
            name='Role',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start', models.DateField()),
                ('end', models.DateField()),
                ('title', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=300)),
                ('job', models.ForeignKey(to='resume.Job')),
            ],
        ),
        migrations.CreateModel(
            name='RoleItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('item', models.CharField(max_length=300)),
                ('role', models.ForeignKey(to='resume.Role')),
            ],
        ),
    ]
