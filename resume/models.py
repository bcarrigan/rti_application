from django.db import models
from django.contrib import admin


class Job(models.Model):
    """
    Defines a single job
    """
    company_name = models.CharField(max_length=30)
    logo_url = models.CharField(max_length=30)
    project_title = models.CharField(max_length=50)
    project_logo = models.CharField(max_length=50)
    project_url = models.CharField(max_length=200)

    def serialize(self):
        return {
            "company": self.company_name,
            "project": {
                "title": self.project_title,
                "url": self.project_url
            },
            "roles": [r.serialize() for r in self.role_set.all()]
        }

    def __unicode__(self):
        return self.company_name


class Role(models.Model):
    """
    Defines a role at a company for when an employee has multiple roles
    at one company.
    """
    job = models.ForeignKey(Job)
    start = models.DateField()
    end = models.DateField(null=True, blank=True)
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=300, blank=True)

    def serialize(self):
        return {
            "start": self.start,
            "end": self.end,
            "title": self.title,
            "responsibilities": [r.serialize() for r in self.roleitem_set.all()]
        }

    def responsibilities(self):
        return self.roleitem_set.all()[::-1]

    def __unicode__(self):
        return "{} at {}".format(self.title, self.job.company_name)


class RoleItem(models.Model):
    """
    Defines a single responsibility or recognition during a given role.
    """
    role = models.ForeignKey(Role)
    item = models.CharField(max_length=300)

    def serialize(self):
        return self.item

    def __unicode__(self):
        return "{}: {}".format(self.role.title, self.item)


class SideProject(models.Model):
    """
    Defines work done on a single side project.
    """
    title = models.CharField(max_length=100)
    image_url = models.CharField(max_length=50)
    description = models.TextField()
    url = models.CharField(max_length=200, null=True, blank=True)

    def serialize(self):
        return {
            "title": self.title,
            "url": self.url,
            "description": self.description
        }

    def __unicode__(self):
        return "{}".format(self.title)


class Education(models.Model):
    """
    Defines one educational degree's track record.
    """
    school = models.CharField(max_length=100)
    image_url = models.CharField(max_length=50)
    start = models.DateField()
    end = models.DateField(null=True, blank=True)
    degree = models.CharField(max_length=80)
    gpa = models.FloatField()

    def serialize(self):
        return {
            "achievements": [a.serialize() for a in self.educationachievement_set.all()],
            "degree": self.degree,
            "gpa": self.gpa,
            "start": self.start,
            "end": self.end,
            "school": self.school
        }

    def __unicode__(self):
        return "{} at {}".format(self.degree, self.school)


class EducationAchievement(models.Model):
    """
    Defines one achievement during an academic record.
    """
    degree = models.ForeignKey(Education)
    item = models.TextField()

    def serialize(self):
        return self.item

    def __unicode__(self):
        return self.item[:20] + "..."


class PersonalInfo(models.Model):
    """
    Defines the personal info for a person.
    """
    image = models.CharField(max_length=50)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    contact_info = models.CharField(max_length=50)
    blurb = models.TextField()
    from_latitude = models.FloatField()
    from_longitude = models.FloatField()

    def serialize(self):
        return {
            "name": self.display_name(),
            "hobbies": [h.serialize() for h in self.hobby_set.all()],
            "contact": self.contact_info,
            "about": self.blurb
        }

    def display_name(self):
        return "{} {}".format(self.first_name, self.last_name)

    def __unicode__(self):
        return self.display_name()


class Hobby(models.Model):
    """
    Defines a single hobby that a person may have.
    """
    personal_info = models.ForeignKey(PersonalInfo)
    name = models.CharField(max_length=30)

    def serialize(self):
        return self.name

    def __unicode__(self):
        return self.name


admin.site.register(Job)
admin.site.register(Role)
admin.site.register(RoleItem)
admin.site.register(SideProject)
admin.site.register(Education)
admin.site.register(EducationAchievement)
admin.site.register(PersonalInfo)
admin.site.register(Hobby)

